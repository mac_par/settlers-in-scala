package com.settlers.production

import java.util.Objects
import java.util.concurrent.TimeUnit

import akka.actor.SupervisorStrategy.Resume
import akka.actor.{ActorRef, OneForOneStrategy, Props, SupervisorStrategy}
import akka.pattern.ask
import akka.routing.RoundRobinPool
import com.settlers.production.FinancialMarket.CoinRequest
import com.settlers.production.market.{Market, MarketRequest, MarketResponse}
import com.settlers.resources.industrial.{CoalOre, GoldOre}
import com.settlers.resources.monetary.Coin
import com.settlers.resources.monetary.factory.Mint

import scala.concurrent.Promise
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

final private[production] class FinancialMarket extends Market {

  private val industrialMarketActor = context.actorOf(RoundRobinPool(3).props(IndustrialMarket.props), "industrial-market-actor2")

  private val mint = Mint()


  //in case of any exception resume work, we dont want to empty the mailbox
  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy(
    maxNrOfRetries = 3, withinTimeRange = Duration(3, TimeUnit.SECONDS)
  ) {
    case ex: Exception =>
      debug(() => s"Exception has occurred: ${ex.getMessage}")
      debug(() => "Resuming work")
      Resume
  }

  //GoldOre, CoalOre
  private val obtainGoldOre: () => Promise[GoldOre] = {
    val promise = Promise[GoldOre]()
    val goldRequest = industrialMarketActor ? IndustrialMarket.GoldOreRequest
    goldRequest.mapTo[IndustrialMarket.GoldOreResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(ex) => error(ex, GoldOre)
        promise failure ex
    }

    () => promise
  }
  private val obtainCoalOre: () => Promise[CoalOre] = {
    val promise = Promise[CoalOre]()
    val coalRequest = industrialMarketActor ? IndustrialMarket.CoalOreRequest
    coalRequest.mapTo[IndustrialMarket.CoalOreResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(ex) => error(ex, CoalOre)
        promise failure ex
    }
    () => promise
  }

  private val produceCoin = (sender: ActorRef) => {
    obtainGoldOre().future.mapTo[GoldOre].onComplete {
      case Success(fstResource) =>
        obtainCoalOre().future.mapTo[CoalOre].onComplete {
          case Success(sndResource) =>
            prepareTuple2Response[GoldOre, CoalOre, Coin, FinancialMarket.CoinResponse](() => fstResource,
              () => sndResource, mint, FinancialMarket.CoinResponse.apply, sender)
          case Failure(exception) => error(exception, CoalOre)
            throwException(CoalOre, exception)
        }
      case Failure(exception) => error(exception, GoldOre)
        throwException(GoldOre, exception)
    }
  }

  override def receive: Receive = {
    //Coin request
    case CoinRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Coin)
      produceCoin(requestedBy)
      debugResponseInfo(Coin)

    case Market.Stop => debug(() => "Stoping actor")
      context.stop(self)
      context.stop(industrialMarketActor)
  }
}

object FinancialMarket {
  private[production] val props = Props[FinancialMarket]

  private[production] case class CoinRequest() extends MarketRequest

  private[production] class CoinResponse(coin: Coin) extends MarketResponse[Coin](coin)

  object CoinResponse {
    private[production] def apply(coin: Coin): CoinResponse = new CoinResponse(coin)

    private[production] def unapply(arg: CoinResponse): Option[Coin] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

}
