package com.settlers.production

import java.util.Objects
import java.util.concurrent.TimeUnit

import akka.actor.SupervisorStrategy.Resume
import akka.actor.{ActorRef, OneForOneStrategy, Props, SupervisorStrategy}
import akka.pattern.ask
import akka.routing.RoundRobinPool
import com.settlers.production.market.{Market, MarketRequest, MarketResponse}
import com.settlers.resources.industrial.factory.{CoalMine, GoldMine, IronMine, SteelMill}
import com.settlers.resources.industrial.{CoalOre, GoldOre, IronOre, Steel}
import com.settlers.resources.urban.{Beer, Bread, Meat}

import scala.concurrent.Promise
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

final private[production] class IndustrialMarket extends Market {

  import com.settlers.production.IndustrialMarket._

  private val urbanMarketActor = context.actorOf(RoundRobinPool(nrOfInstances = 7).props(UrbanMarket.props), "urban-market-actor")

  private val coalMine = CoalMine()
  private val goldMine = GoldMine()
  private val ironMine = IronMine()
  private val steelMill = SteelMill()


  //in case of any exception resume work, we dont want to empty the mailbox
  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy(
    maxNrOfRetries = 3, withinTimeRange = Duration(3, TimeUnit.SECONDS)
  ) {
    case ex: Exception =>
      debug(() => s"Exception has occurred: ${ex.getMessage}")
      debug(() => "Resuming work")
      Resume
  }

  private val obtainMeat: () => Promise[Meat] = {
    val promise = Promise[Meat]()
    val meatRequest = urbanMarketActor ? UrbanMarket.MeatRequest
    meatRequest.mapTo[UrbanMarket.MeatResponse].onComplete {
      case Success(value) =>
        promise success value.resource
      case Failure(exception) => error(exception, Meat)
        promise failure exception
    }
    () => promise
  }
  private val obtainBeer: () => Promise[Beer] = {
    val promise = Promise[Beer]()
    val grainRequest = urbanMarketActor ? UrbanMarket.BeerRequest
    grainRequest.mapTo[UrbanMarket.BeerResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(exception) => error(exception, Beer)
        promise failure exception
    }
    () => promise
  }

  private val obtainBread: () => Promise[Bread] = {
    val promise = Promise[Bread]()
    val grainRequest = urbanMarketActor ? UrbanMarket.BreadRequest
    grainRequest.mapTo[UrbanMarket.BreadResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(exception) => error(exception, Bread)
        promise failure exception
    }
    () => promise
  }

  private val obtainCoalOre: () => Promise[CoalOre] = {
    val promise = Promise[CoalOre]()
    obtainMeat().future.mapTo[Meat].onComplete {
      case Success(meat) => {
        obtainBeer().future.mapTo[Beer].onComplete {
          case Success(beer) =>
            promise success coalMine.produce((meat, beer))
          case Failure(exception) => error(exception, Beer)
            promise failure exception
        }
      }
      case Failure(exception) => error(exception, Meat)
        promise failure exception
    }
    () => promise
  }

  private val obtainIronOre: () => Promise[IronOre] = {
    val promise = Promise[IronOre]()
    obtainMeat().future.mapTo[Meat].onComplete {
      case Success(meat) => {
        obtainBeer().future.mapTo[Beer].onComplete {
          case Success(beer) => val resource = ironMine.produce((meat, beer))
            promise success resource
          case Failure(exception) => error(exception, Beer)
            promise failure exception
        }
      }
      case Failure(exception) => error(exception, Meat)
        promise failure exception
    }
    () => promise
  }

  private val produceCoalOre = (sender: ActorRef) => {
    obtainMeat().future.mapTo[Meat].onComplete {
      case Success(fstResource) =>
        obtainBeer().future.mapTo[Beer].onComplete {
          case Success(sndResource) =>
            prepareTuple2Response[Meat, Beer, CoalOre, CoalOreResponse](() => fstResource, () => sndResource, coalMine,
              CoalOreResponse.apply, sender)
          case Failure(exception) => errorMsg(Beer)
            throwException(Beer, exception)
        }
      case Failure(exception) => errorMsg(Meat)
        throwException(Meat, exception)
    }
  }

  private val produceGoldOre = (sender: ActorRef) => {
    obtainBread().future.mapTo[Bread].onComplete {
      case Success(fstResource) =>
        obtainBeer().future.mapTo[Beer].onComplete {
          case Success(sndResource) =>
            prepareTuple2Response[Bread, Beer, GoldOre, GoldOreResponse](() => fstResource, () => sndResource, goldMine,
              GoldOreResponse.apply, sender)
          case Failure(exception) => errorMsg(Beer)
            throwException(Beer, exception)
        }
      case Failure(exception) => errorMsg(Bread)
        throwException(Bread, exception)
    }
  }

  private val produceIronOre = (sender: ActorRef) => {
    obtainMeat().future.mapTo[Meat].onComplete {
      case Success(fstResource) =>
        obtainBeer().future.mapTo[Beer].onComplete {
          case Success(sndResource) =>
            prepareTuple2Response[Meat, Beer, IronOre, IronOreResponse](() => fstResource, () => sndResource, ironMine,
              IronOreResponse.apply, sender)
          case Failure(exception) => errorMsg(Beer)
            throwException(Beer, exception)
        }
      case Failure(exception) => errorMsg(Meat)
        throwException(Meat, exception)
    }
  }

  private val produceSteel = (sender: ActorRef) => {
    obtainCoalOre().future.mapTo[CoalOre].onComplete {
      case Success(coalOre) => {
        obtainIronOre().future.mapTo[IronOre].onComplete {
          case Success(ironOre) =>
            val coal = () => coalOre
            val iron = () => ironOre
            prepareTuple2Response[CoalOre, IronOre, Steel, SteelResponse](coal, iron, steelMill, SteelResponse.apply, sender)
          case Failure(exception) => error(exception, Beer)
            throw exception
        }
      }
      case Failure(exception) => error(exception, Meat)
        throw exception
    }
  }

  override def receive: Receive = {
    //CoalOre request
    case CoalOreRequest =>
      val requestedBy = currentSender
      debugRequestInfo(CoalOre)
      produceCoalOre(requestedBy)
      debugResponseInfo(CoalOre)

    //GoldOre request
    case GoldOreRequest =>
      val requestedBy = currentSender
      debugRequestInfo(GoldOre)
      produceGoldOre(requestedBy)
      debugResponseInfo(GoldOre)

    //IronOre request
    case IronOreRequest =>
      val requestedBy = currentSender
      debugRequestInfo(IronOre)
      produceIronOre(requestedBy)
      debugResponseInfo(IronOre)

    //Steel Request
    case SteelRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Steel)
      produceSteel(requestedBy)
      debugResponseInfo(Steel)

    case Market.Stop => debug(() => "Stoping actor")
      context.stop(self)
      context.stop(urbanMarketActor)
  }
}

object IndustrialMarket {
  def apply(): IndustrialMarket = new IndustrialMarket()

  private[production] val props = Props[IndustrialMarket].withDispatcher("general-market-dispatcher")

  private[production] case class CoalOreRequest() extends MarketRequest

  private[production] class CoalOreResponse(coalOre: CoalOre) extends MarketResponse[CoalOre](coalOre) {
  }

  object CoalOreResponse {
    private[production] def apply(coalOre: CoalOre): CoalOreResponse = new CoalOreResponse(coalOre)

    private[production] def unapply(arg: CoalOreResponse): Option[CoalOre] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class GoldOreRequest() extends MarketRequest


  private[production] class GoldOreResponse(goldOre: GoldOre) extends MarketResponse[GoldOre](goldOre) {
  }

  object GoldOreResponse {
    private[production] def apply(goldOre: GoldOre): GoldOreResponse = new GoldOreResponse(goldOre)

    private[production] def unapply(arg: GoldOreResponse): Option[GoldOre] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class IronOreRequest() extends MarketRequest

  private[production] class IronOreResponse(ironOre: IronOre) extends MarketResponse[IronOre](ironOre) {
  }

  object IronOreResponse {
    private[production] def apply(ironOre: IronOre): IronOreResponse = new IronOreResponse(ironOre)

    private[production] def unapply(arg: IronOreResponse): Option[IronOre] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class SteelRequest() extends MarketRequest

  private[production] class SteelResponse(steel: Steel) extends MarketResponse[Steel](steel) {
  }

  object SteelResponse {
    private[production] def apply(steel: Steel): SteelResponse = new SteelResponse(steel)

    private[production] def unapply(arg: SteelResponse): Option[Steel] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

}
