package com.settlers.production.market

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef}
import akka.event.Logging
import akka.pattern.{PipeableFuture, ask}
import akka.util.Timeout
import com.settlers.resources.urban.Bread
import com.settlers.resources.{BiFactory, MissingResourcesException, Nameable, Resource}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success}

trait Market extends Actor {
  protected val log = Logging(context.system, this)
  protected implicit val dispatcher: ExecutionContext = context.dispatcher
  protected implicit val timeout: Timeout = Timeout(1000, TimeUnit.MILLISECONDS)

  protected def currentSender: ActorRef = context.sender()

  //A: resource, B: response case class
  protected def prepareResponse[A <: Resource, B <: MarketResponse[A]]
  (resource: () => A, function: A => B, sender: ActorRef)
  : PipeableFuture[B] = {
    akka.pattern.pipe {
      Future {
        function(resource())
      }
    } to sender
  }

  //A: subresource, B: final resource, C - response type
  protected def prepareResponse[A <: Resource, B <: Resource, C <: MarketResponse[B]]
  (subresource: () => A, resource: BiFactory[A, B], function: B => C, sender: ActorRef): PipeableFuture[C] = {
    akka.pattern.pipe {
      Future {
        val subRes = resource.produce(subresource())
        function(subRes)
      }
    } to sender
  }

  //A: first resource, B: second resource, C - response type with Tuple2(A,B)
  protected def prepareTuple2Response[A <: Resource, B <: Resource, C <: Resource, D <: MarketResponse[C]]
  (fstResource: () => A, sndResource: () => B, factory: BiFactory[(A, B), C], function: C => D,
   sender: ActorRef): PipeableFuture[D] = {
    akka.pattern.pipe {
      Future {
        val resource = (fstResource(), sndResource())
        val anotherResource = factory.produce(resource)
        function(anotherResource)
      }
    } to sender
  }

  //A: initial resouce, B: subresource, C: final resource, D - response type
  protected def prepareResponse[A <: Resource, B <: Resource, C <: Resource, D <: MarketResponse[C]]
  (initial: () => A, subResource: BiFactory[A, B], resource: BiFactory[B, C], function: C => D, sender: ActorRef):
  PipeableFuture[D] = {
    akka.pattern.pipe {
      Future {
        val sub = subResource.produce(initial())
        val finalResource = resource.produce(sub)
        function(finalResource)
      }
    } to sender
  }

  //maybe to expensive
  protected def sendResponseWithSinglePromise[A <: Resource, B <: Resource]
  (promiseFn1: () => Promise[A], factory: BiFactory[A, B],
   wrapFn: B => MarketResponse[B], endResource: Nameable, sender: ActorRef): Unit = {
    promiseFn1().future.onComplete {
      case Success(fstResource) =>
        prepareResponse[A, B, MarketResponse[B]](() => fstResource, factory, wrapFn, sender)
      case Failure(exception) => errorMsg(endResource)
        throwException(Bread, exception)
    }
  }

  protected def debug(msg: () => String): Unit = log.info(msg())

  protected def error(cause: Throwable, resource: Nameable): Unit = log.error(cause, errorMsg(resource)())

  protected def debugRequestInfo(resource: Nameable): Unit = debug(requestInfo(resource))

  protected def debugResponseInfo(resource: Nameable): Unit = debug(responseInfo(resource))

  protected def requestInfo(resource: Nameable): () => String =
    () => s"${resource.name} was requested by ${context.sender}"

  protected def responseInfo(resource: Nameable): () => String = () => s"Sending ${resource.name} back to ${context.sender}"

  protected def throwException(resource: Nameable, cause: Throwable): Unit =
    throw new MissingResourcesException(errorMsg(resource), cause)

  protected def errorMsg(resource: Nameable): () => String = () => s"${resource.name} could not be obtained"
}

object Market {

  case class Stop()
}
