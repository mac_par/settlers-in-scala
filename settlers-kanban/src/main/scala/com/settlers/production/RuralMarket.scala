package com.settlers.production

import java.util.Objects

import akka.actor.Props
import com.settlers.production.market.{Market, MarketRequest, MarketResponse}
import com.settlers.resources.agriculture.factory.{Farm, Mill, Well}
import com.settlers.resources.agriculture.{Flour, Grain, Water}

final private[production] class RuralMarket extends Market {

  import com.settlers.production.RuralMarket._

  private val farm: Farm = Farm()
  private val well: Well = Well()
  private val mill: Mill = Mill()

  private val produceGrain = () => farm.produce()
  private val produceWater = () => well.produce()

  override def receive: Receive = {
    //flour request
    case FlourRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Flour)
      prepareResponse[Grain, Flour, FlourResponse](produceGrain, mill, FlourResponse.apply,
        requestedBy)
      debugResponseInfo(Flour)

    //grain request
    case GrainRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Grain)
      prepareResponse[Grain, GrainResponse](produceGrain, GrainResponse.apply, requestedBy)
      debugResponseInfo(Grain)

    //Water request
    case WaterRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Water)
      prepareResponse[Water, WaterResponse](produceWater, WaterResponse.apply, requestedBy)
      debugResponseInfo(Water)

    case Market.Stop => debug(() => "Stoping actor")
      context.stop(self)
  }
}

object RuralMarket {
  def apply(): RuralMarket = new RuralMarket()

  private[production] val props = Props[RuralMarket].withDispatcher("general-market-dispatcher")

  private[production] case class FlourRequest() extends MarketRequest

  private[production] class FlourResponse(flour: Flour) extends MarketResponse[Flour](flour) {
  }

  object FlourResponse {
    private[production] def apply(flour: Flour): FlourResponse = new FlourResponse(flour)

    private[production] def unapply(arg: FlourResponse): Option[Flour] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class GrainRequest() extends MarketRequest

  private[production] class GrainResponse(grain: Grain) extends MarketResponse[Grain](grain) {
  }

  object GrainResponse {
    private[production] def apply(grain: Grain): GrainResponse = new GrainResponse(grain)

    private[production] def unapply(arg: GrainResponse): Option[Grain] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class WaterRequest() extends MarketRequest

  private[production] class WaterResponse(water: Water) extends MarketResponse[Water](water) {
  }

  object WaterResponse {
    private[production] def apply(water: Water): WaterResponse = new WaterResponse(water)

    private[production] def unapply(arg: WaterResponse): Option[Water] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

}
