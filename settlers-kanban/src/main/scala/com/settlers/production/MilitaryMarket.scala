package com.settlers.production

import java.util.Objects
import java.util.concurrent.TimeUnit

import akka.actor.SupervisorStrategy.Resume
import akka.actor.{ActorRef, OneForOneStrategy, Props, SupervisorStrategy}
import akka.pattern.ask
import akka.routing.RoundRobinPool
import com.settlers.production.market.{Market, MarketRequest, MarketResponse}
import com.settlers.resources.forestry.Plank
import com.settlers.resources.industrial.{CoalOre, Steel}
import com.settlers.resources.military.factory.{Barracks, BlackSmith}
import com.settlers.resources.military.{Shield, Soldier, Sword}
import com.settlers.resources.monetary.Coin

import scala.concurrent.Promise
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

final private[production] class MilitaryMarket extends Market {

  import com.settlers.production.MilitaryMarket._

  private val forestryMarketActor = context.actorOf(RoundRobinPool(6).props(ForestryMarket.props), "forestry-market-actor")
  private val industryMarketActor = context.actorOf(RoundRobinPool(9).props(IndustrialMarket.props), "industrial-market-actor")
  private val financialMarketActor = context.actorOf(RoundRobinPool(6).props(FinancialMarket.props), "financial-market-actor")

  private val blackSmith = BlackSmith()
  private val barracks = Barracks()

  //in case of any exception resume work, we dont want to empty the mailbox
  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy(
    maxNrOfRetries = 3, withinTimeRange = Duration(3, TimeUnit.SECONDS)
  ) {
    case ex: Exception =>
      debug(() => s"Exception has occurred: ${ex.getMessage}")
      debug(() => "Resuming work")
      Resume
  }

  private val obtainCoin: () => Promise[Coin] = {
    val promise = Promise[Coin]()
    val coinRequest = financialMarketActor ? FinancialMarket.CoinRequest
    coinRequest.mapTo[FinancialMarket.CoinResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(ex) => error(ex, Coin)
        promise failure ex
    }

    () => promise
  }

  private val obtainSteel: () => Promise[Steel] = {
    val promise = Promise[Steel]()
    val steelRequest = industryMarketActor ? IndustrialMarket.SteelRequest
    steelRequest.mapTo[IndustrialMarket.SteelResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(ex) => error(ex, Steel)
        promise failure ex
    }

    () => promise
  }

  private val obtainCoal: () => Promise[CoalOre] = {
    val promise = Promise[CoalOre]()
    val coalRequest = industryMarketActor ? IndustrialMarket.CoalOreRequest
    coalRequest.mapTo[IndustrialMarket.CoalOreResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(ex) => error(ex, CoalOre)
        promise failure ex
    }
    () => promise
  }

  private val obtainPlank: () => Promise[Plank] = {
    val promise = Promise[Plank]()
    val plankRequest = forestryMarketActor ? ForestryMarket.PlankRequest
    plankRequest.mapTo[ForestryMarket.PlankResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(ex) => error(ex, Plank)
        promise failure ex
    }
    () => promise
  }

  private val obtainSword: () => Promise[Sword] = {
    val promise = Promise[Sword]()
    obtainSteel().future.mapTo[Steel].onComplete {
      case Success(fstResource) =>
        obtainCoal().future.mapTo[CoalOre].onComplete {
          case Success(sndResource) =>
            val sword = blackSmith.produce((fstResource, sndResource))
            promise success sword
          case Failure(exception) => errorMsg(CoalOre)
            promise failure exception
        }
      case Failure(exception) => errorMsg(Steel)
        promise failure exception
    }
    () => promise
  }

  private val obtainShield: () => Promise[Shield] = {
    val promise = Promise[Shield]()
    obtainSteel().future.mapTo[Steel].onComplete {
      case Success(fstResource) =>
        obtainCoal().future.mapTo[CoalOre].onComplete {
          case Success(sndResource) =>
            obtainPlank().future.mapTo[Plank].onComplete {
              case Success(thdResource) =>
                val shield = blackSmith.produce((fstResource, sndResource, thdResource))
                promise success shield
              case Failure(exception) => error(exception, Plank)
                promise failure exception
            }
          case Failure(exception) => error(exception, CoalOre)
            promise failure exception
        }
      case Failure(exception) => error(exception, Steel)
        promise failure exception
    }

    () => promise
  }

  private val produceSword = (sender: ActorRef) => {
    obtainSword().future.mapTo[Sword].onComplete {
      case Success(sword) =>
        prepareResponse[Sword, SwordResponse](() => sword,
          MilitaryMarket.SwordResponse.apply, sender)
      case Failure(exception) => error(exception, Sword)
        throwException(Sword, exception)
    }
  }

  private val produceSoldier = (sender: ActorRef) => {
    obtainSword().future.mapTo[Sword].onComplete {
      case Success(sword) =>
        obtainCoin().future.mapTo[Coin].onComplete {
          case Success(coin) =>
            obtainShield().future.mapTo[Shield].onComplete {
              case Success(shield) =>
                val soldier = barracks.produce((sword, coin, shield))
                prepareResponse[Soldier, SoldierResponse](() => soldier, MilitaryMarket.SoldierResponse.apply, sender)
              case Failure(exception) => error(exception, Coin)
                throwException(Coin, exception)
            }
          case Failure(exception) => error(exception, Coin)
            throwException(Coin, exception)
        }
      case Failure(exception) => error(exception, Sword)
        throwException(Sword, exception)
    }
  }

  private val produceShield = (sender: ActorRef) => {
    obtainShield().future.mapTo[Shield].onComplete {
      case Success(shield) =>
        prepareResponse[Shield, ShieldResponse](() => shield, MilitaryMarket.ShieldResponse.apply, sender)
      case Failure(exception) => error(exception, Steel)
        throwException(Steel, exception)
    }
  }

  override def receive: Receive = {

    case SoldierRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Soldier)
      produceSoldier(requestedBy)
      debugResponseInfo(Soldier)

    case ShieldRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Shield)
      produceShield(requestedBy)
      debugResponseInfo(Shield)

    case SwordRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Sword)
      produceSword(requestedBy)
      debugResponseInfo(Sword)

    case Market.Stop => debug(() => "Stoping actor")
      context.stop(self)
      context.stop(forestryMarketActor)
      context.stop(industryMarketActor)
      context.stop(financialMarketActor)
  }
}

object MilitaryMarket {
  private[production] val props = Props[MilitaryMarket]

  private[production] case class SoldierRequest() extends MarketRequest

  private[production] class SoldierResponse(soldier: Soldier) extends MarketResponse(soldier)

  object SoldierResponse {
    private[production] def apply(soldier: Soldier): SoldierResponse = new SoldierResponse(soldier)

    private[production] def unapply(arg: SoldierResponse): Option[Soldier] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class ShieldRequest() extends MarketRequest

  private[production] class ShieldResponse(shield: Shield) extends MarketResponse(shield)

  object ShieldResponse {
    private[production] def apply(shield: Shield): ShieldResponse = new ShieldResponse(shield)

    private[production] def unapply(arg: ShieldResponse): Option[Shield] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class SwordRequest() extends MarketRequest

  private[production] class SwordResponse(sword: Sword) extends MarketResponse(sword)

  object SwordResponse {
    private[production] def apply(sword: Sword): SwordResponse = new SwordResponse(sword)

    private[production] def unapply(arg: SwordResponse): Option[Sword] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

}
