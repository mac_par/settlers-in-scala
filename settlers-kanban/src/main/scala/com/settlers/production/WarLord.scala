package com.settlers.production

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, PoisonPill}
import akka.pattern.ask
import akka.util.Timeout
import com.settlers.resources.military.Soldier

import scala.concurrent.Promise
import scala.util.{Failure, Success}

final class WarLord extends java.io.Closeable {
  private val systemActor = ActorSystem("warlord-main-system")
  private val soldierActor = systemActor.actorOf(MilitaryMarket.props, "soldier-producer-actor")
  private implicit val timeout = Timeout(2000, TimeUnit.MILLISECONDS)

  val promiseMeASoldat = () => {
    val promise = Promise[Soldier]()

    val soldierResquest = soldierActor ? MilitaryMarket.SoldierRequest
    soldierResquest.mapTo[MilitaryMarket.SoldierResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(exception) => promise failure exception
    }(systemActor.dispatcher)

    promise
  }

  final override def close(): Unit = {
    soldierActor ! PoisonPill
    systemActor.terminate()
  }
}
