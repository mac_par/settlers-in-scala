package com.settlers.production

import java.util.Objects
import java.util.concurrent.TimeUnit

import akka.actor.SupervisorStrategy.Resume
import akka.actor.{ActorRef, OneForOneStrategy, Props, SupervisorStrategy}
import akka.pattern._
import akka.routing.RoundRobinPool
import com.settlers.production.market.{Market, MarketRequest, MarketResponse}
import com.settlers.resources.agriculture.{Flour, Grain, Water}
import com.settlers.resources.urban.factory.{Bakery, Brewery, Butcher, Pigsty}
import com.settlers.resources.urban.{Beer, Bread, Meat, Pig}

import scala.concurrent.Promise
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

final private[production] class UrbanMarket extends Market {

  import com.settlers.production.UrbanMarket._

  private val ruralMarketActor = context.actorOf(RoundRobinPool(8).props(RuralMarket.props), "rural-market")

  private val brewery = Brewery()
  private val bakery = Bakery()
  private val pigsty = Pigsty()
  private val butcher = Butcher()

  //in case of any exception resume work, we dont want to empty the mailbox
  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy(
    maxNrOfRetries = 3, withinTimeRange = Duration(3, TimeUnit.SECONDS)
  ) {
    case ex: Exception =>
      debug(() => s"Exception has occurred: ${ex.getMessage}")
      debug(() => "Resuming work")
      Resume
  }

  private val obtainGrain: () => Promise[Grain] = {
    val promise = Promise[Grain]()
    val grainRequest = ruralMarketActor ? RuralMarket.GrainRequest
    grainRequest.mapTo[RuralMarket.GrainResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(exception) => error(exception, Grain)
        promise failure exception
    }
    () => promise
  }
  private val obtainFlour: () => Promise[Flour] = {
    val promise = Promise[Flour]()
    val flourRequest = ruralMarketActor ? RuralMarket.FlourRequest
    flourRequest.mapTo[RuralMarket.FlourResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(exception) => error(exception, Flour)
        promise failure exception
    }

    () => promise
  }

  private val obtainWater: () => Promise[Water] = {
    val promise = Promise[Water]()
    val waterRequest = ruralMarketActor ? RuralMarket.WaterRequest
    waterRequest.mapTo[RuralMarket.WaterResponse].onComplete {
      case Success(value) => promise success value.resource
      case Failure(exception) => error(exception, Water)
        promise failure exception
    }

    () => promise
  }

  private val produceBeer = (sender: ActorRef) => {
    val grainPromise = obtainGrain()
    val waterPromise = obtainWater()

    waterPromise.future.onComplete {
      //water
      case Success(water) => {
        //flour
        grainPromise.future.onComplete {
          case Success(grain) =>
            prepareTuple2Response[Grain, Water, Beer, BeerResponse](() => grain, () => water,
              brewery, BeerResponse.apply, sender)
          case Failure(exception) => error(exception, Beer)
            throwException(Beer, exception)
        }
      }
      case Failure(exception) => error(exception, Bread)
        throwException(Bread, exception)
    }
  }

  private val producePig = (sender: ActorRef) => {
    obtainGrain().future onComplete {
      case Success(value) =>
        prepareResponse[Pig, PigResponse](() => pigsty.produce(value), PigResponse.apply, sender: ActorRef)
      case Failure(exception) => error(exception, Pig)
        throwException(Pig, exception)
    }
  }

  private val produceBread = (sender: ActorRef) => {
    val flourPromise = obtainFlour()
    val waterPromise = obtainWater()

    waterPromise.future.onComplete {
      //water
      case Success(water) => {
        //flour
        flourPromise.future.onComplete {
          case Success(flour) =>
            prepareResponse[Bread, BreadResponse](() => bakery.produce((flour, water)), BreadResponse.apply, sender)
          case Failure(exception) => error(exception, Bread)
            throwException(Bread, exception)
        }
      }
      case Failure(exception) => error(exception, Bread)
        throwException(Bread, exception)
    }
  }

  private val produceMeat = (sender: ActorRef) => {
    obtainGrain().future onComplete {
      case Success(value) =>
        prepareResponse[Pig, Meat, MeatResponse](() => pigsty.produce(value), butcher,
          MeatResponse.apply, sender)
      case Failure(exception) => error(exception, Meat)
        throwException(Meat, exception)
    }
  }

  override def receive: Receive = {
    //Beer request
    case BeerRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Beer)
      produceBeer(requestedBy)
      debugResponseInfo(Beer)

    //Bread request
    case BreadRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Bread)
      produceBread(requestedBy)
      debugResponseInfo(Bread)

    //Pig request
    case PigRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Pig)
      producePig(requestedBy)
      debugResponseInfo(Pig)

    //Meat request
    case MeatRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Meat)
      produceMeat(requestedBy)
      debugResponseInfo(Meat)

    case Market.Stop => debug(() => "Stoping actor")
      context.stop(self)
      context.stop(ruralMarketActor)
  }
}

object UrbanMarket {
  def apply(): UrbanMarket = new UrbanMarket()

  private[production] val props = Props[UrbanMarket].withDispatcher("general-market-dispatcher")

  private[production] case class BeerRequest() extends MarketRequest

  private[production] class BeerResponse(beer: Beer) extends MarketResponse[Beer](beer) {
  }

  object BeerResponse {
    private[production] def apply(beer: Beer): BeerResponse = new BeerResponse(beer)

    private[production] def unapply(arg: BeerResponse): Option[Beer] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class BreadRequest() extends MarketRequest

  private[production] class BreadResponse(bread: Bread) extends MarketResponse[Bread](bread) {
  }

  object BreadResponse {
    private[production] def apply(bread: Bread): BreadResponse = new BreadResponse(bread)

    private[production] def unapply(arg: BreadResponse): Option[Bread] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class PigRequest() extends MarketRequest

  private[production] class PigResponse(pig: Pig) extends MarketResponse[Pig](pig) {
  }

  object PigResponse {
    private[production] def apply(pig: Pig): PigResponse = new PigResponse(pig)

    private[production] def unapply(arg: PigResponse): Option[Pig] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class MeatRequest() extends MarketRequest

  private[production] class MeatResponse(meat: Meat) extends MarketResponse[Meat](meat) {
  }

  object MeatResponse {
    private[production] def apply(meat: Meat): MeatResponse = new MeatResponse(meat)

    private[production] def unapply(arg: MeatResponse): Option[Meat] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

}
