package com.settlers.production

import java.util.Objects

import akka.actor.Props
import com.settlers.production.market.{Market, MarketRequest, MarketResponse}
import com.settlers.resources.forestry.factory._
import com.settlers.resources.forestry.{Plank, Tree, Wood}

final private[production] class ForestryMarket extends Market {

  import com.settlers.production.ForestryMarket._

  private val forest: Forest = Forest()
  private val cutter: Cutter = Cutter()
  private val sawMill: SawMill = SawMill()

  private val produceTree = () => forest.produce()

  override def receive: Receive = {
    //Plank request
    case PlankRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Plank)
      prepareResponse[Tree, Wood, Plank, PlankResponse](produceTree, cutter, sawMill, PlankResponse.apply, requestedBy)
      debugResponseInfo(Plank)

    //Tree request
    case TreeRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Tree)
      prepareResponse[Tree, TreeResponse](produceTree, TreeResponse.apply, requestedBy)
      debugResponseInfo(Tree)

    //Wood resquest
    case WoodRequest =>
      val requestedBy = currentSender
      debugRequestInfo(Wood)
      prepareResponse[Tree, Wood, WoodResponse](produceTree, cutter, WoodResponse.apply, requestedBy)
      debugResponseInfo(Wood)

    case Market.Stop => debug(() => "Stoping actor")
      context.stop(self)
  }
}

object ForestryMarket {
  def apply(): ForestryMarket = new ForestryMarket()

  private[production] val props = Props[ForestryMarket]

  private[production] case class PlankRequest() extends MarketRequest

  private[production] class PlankResponse(plank: Plank) extends MarketResponse[Plank](plank) {
  }

  object PlankResponse {
    private[production] def apply(plank: Plank): PlankResponse = new PlankResponse(plank)

    private[production] def unapply(arg: PlankResponse): Option[Plank] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

  private[production] case class TreeRequest() extends MarketRequest

  private[production] class TreeResponse(tree: Tree) extends MarketResponse[Tree](tree) {
  }

  object TreeResponse {
    private[production] def apply(tree: Tree): TreeResponse = new TreeResponse(tree)

    private[production] def unapply(arg: TreeResponse): Option[Tree] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }


  private[production] case class WoodRequest() extends MarketRequest

  private[production] class WoodResponse(wood: Wood) extends MarketResponse[Wood](wood) {
  }

  object WoodResponse {
    private[production] def apply(wood: Wood): WoodResponse = new WoodResponse(wood)

    private[production] def unapply(arg: WoodResponse): Option[Wood] =
      if (Objects.isNull(arg)) None else Some(arg.resource)
  }

}
