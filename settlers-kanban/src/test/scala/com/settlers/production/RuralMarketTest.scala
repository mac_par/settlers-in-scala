package com.settlers.production

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern._
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import com.settlers.resources.agriculture.{Flour, Grain, Water}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.{BeforeAndAfterAll, GivenWhenThen}

import scala.concurrent.ExecutionContext

class RuralMarketTest extends TestKit(ActorSystem("test-market")) with ImplicitSender with AnyWordSpecLike
  with Matchers with BeforeAndAfterAll with GivenWhenThen {

  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  private implicit val timeout: Timeout = Timeout(1000, TimeUnit.MILLISECONDS)
  private implicit val ec: ExecutionContext = ExecutionContext.global

  "Rural Market" must {
    "Water must be returned on request" in {
      Given("Prepared Rural Market actor and request")
      val actor = system.actorOf(RuralMarket.props, "test-rural-market1")
      val request: RuralMarket.WaterRequest = RuralMarket.WaterRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Water.getClass}")
      for {
        value <- response.mapTo[RuralMarket.WaterResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Water]
      }
    }

    "Grain must be returned on request" in {
      Given("Prepared Rural Market actor and request")
      val actor = system.actorOf(RuralMarket.props, "test-rural-market2")
      val request: RuralMarket.GrainRequest = RuralMarket.GrainRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Grain.getClass}")
      for {
        value <- response.mapTo[RuralMarket.GrainResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Grain]
      }
    }

    "Flour must be returned on request" in {
      Given("Prepared Rural Market actor and request")
      val actor = system.actorOf(RuralMarket.props, "test-rural-market3")
      val request: RuralMarket.FlourRequest = RuralMarket.FlourRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Flour.getClass}")
      for {
        value <- response.mapTo[RuralMarket.FlourResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Flour]
      }
    }
  }
}
