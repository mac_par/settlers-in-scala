package com.settlers.production

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import com.settlers.resources.industrial.{CoalOre, GoldOre, IronOre, Steel}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.{BeforeAndAfterAll, GivenWhenThen}

import scala.concurrent.ExecutionContext

class IndustrialMarketTest extends TestKit(ActorSystem("test-system-actor")) with ImplicitSender with Matchers
  with GivenWhenThen with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  private implicit val timeout: Timeout = Timeout(1000, TimeUnit.MILLISECONDS)
  private implicit val ec: ExecutionContext = ExecutionContext.global

  "Industrial Market" must {
    "CoalOre must be returned on request" in {
      Given("Prepared Industrial Market actor and request")
      val actor = system.actorOf(IndustrialMarket.props, "test-industrial-market1")
      val request: IndustrialMarket.CoalOreRequest = IndustrialMarket.CoalOreRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${CoalOre.getClass}")
      for {
        value <- response.mapTo[IndustrialMarket.CoalOreResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[CoalOre]
      }
    }

    "GoldOre must be returned on request" in {
      Given("Prepared Industrial Market actor and request")
      val actor = system.actorOf(IndustrialMarket.props, "test-industrial-market2")
      val request: IndustrialMarket.GoldOreRequest = IndustrialMarket.GoldOreRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${GoldOre.getClass}")
      for {
        value <- response.mapTo[IndustrialMarket.GoldOreResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[GoldOre]
      }
    }

    "IronOre must be returned on request" in {
      Given("Prepared Industrial Market actor and request")
      val actor = system.actorOf(IndustrialMarket.props, "test-Industrial-market3")
      val request: IndustrialMarket.IronOreRequest = IndustrialMarket.IronOreRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${IronOre.getClass}")
      for {
        value <- response.mapTo[IndustrialMarket.IronOreResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[IronOre]
      }
    }

    "Steel must be returned on request" in {
      Given("Prepared Industrial Market actor and request")
      val actor = system.actorOf(IndustrialMarket.props, "test-industrial-market4")
      val request: IndustrialMarket.SteelRequest = IndustrialMarket.SteelRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Steel.getClass}")
      for {
        value <- response.mapTo[IndustrialMarket.SteelResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Steel]
      }
    }
  }
}
