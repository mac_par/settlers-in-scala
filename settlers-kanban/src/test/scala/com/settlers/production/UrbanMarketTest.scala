package com.settlers.production

import java.util.concurrent.TimeUnit

import akka.pattern.ask
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import com.settlers.resources.urban.{Beer, Bread, Meat, Pig}
import org.scalatest.{BeforeAndAfterAll, GivenWhenThen}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent.ExecutionContext

class UrbanMarketTest extends TestKit(ActorSystem("test-urban-actor")) with AnyWordSpecLike with ImplicitSender
  with Matchers with GivenWhenThen with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  private implicit val timeout: Timeout = Timeout(1000, TimeUnit.MILLISECONDS)
  private implicit val ec: ExecutionContext = ExecutionContext.global

  "Urban Market" must {
    "Bread must be returned on request" in {
      Given("Prepared Urban Market actor and request")
      val actor = system.actorOf(UrbanMarket.props, "test-urban-market1")
      val request: UrbanMarket.BreadRequest = UrbanMarket.BreadRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Bread.getClass}")
      for {
        value <- response.mapTo[UrbanMarket.BreadResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Bread]
      }
    }

    "Beer must be returned on request" in {
      Given("Prepared Urban Market actor and request")
      val actor = system.actorOf(UrbanMarket.props, "test-urban-market2")
      val request: UrbanMarket.BeerRequest = UrbanMarket.BeerRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Beer.getClass}")
      for {
        value <- response.mapTo[UrbanMarket.BeerResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Beer]
      }
    }

    "Pig must be returned on request" in {
      Given("Prepared Urban Market actor and request")
      val actor = system.actorOf(UrbanMarket.props, "test-urban-market3")
      val request: UrbanMarket.PigRequest = UrbanMarket.PigRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Pig.getClass}")
      for {
        value <- response.mapTo[UrbanMarket.PigResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Pig]
      }
    }

    "Meat must be returned on request" in {
      Given("Prepared Urban Market actor and request")
      val actor = system.actorOf(UrbanMarket.props, "test-urban-market4")
      val request: UrbanMarket.MeatRequest = UrbanMarket.MeatRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Meat.getClass}")
      for {
        value <- response.mapTo[UrbanMarket.MeatResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Meat]
      }
    }
  }
}
