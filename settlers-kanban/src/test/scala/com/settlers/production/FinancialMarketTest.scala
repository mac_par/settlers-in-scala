package com.settlers.production

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import com.settlers.resources.monetary.Coin
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.{BeforeAndAfterAll, GivenWhenThen}

import scala.concurrent.ExecutionContext

class FinancialMarketTest extends TestKit(ActorSystem("financial-market-test-actor")) with AnyWordSpecLike
  with Matchers with ImplicitSender with GivenWhenThen with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  private implicit val timeout: Timeout = Timeout(1000, TimeUnit.MILLISECONDS)
  private implicit val ec: ExecutionContext = ExecutionContext.global

  "Financial Market" must {
    "Coin must be returned on request" in {
      Given("Prepared Financial Market actor and request")
      val actor = system.actorOf(FinancialMarket.props, "test-Financial-market")
      val request: FinancialMarket.CoinRequest = FinancialMarket.CoinRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Coin.getClass}")
      for {
        value <- response.mapTo[FinancialMarket.CoinResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Coin]
      }
    }
  }
}
