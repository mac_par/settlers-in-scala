package com.settlers.production

import java.util.concurrent.TimeUnit

import akka.pattern._
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import com.settlers.resources.forestry.{Plank, Tree, Wood}
import org.scalatest.{BeforeAndAfterAll, GivenWhenThen}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent.ExecutionContext


class ForestryMarketTest extends TestKit(ActorSystem("test-market")) with ImplicitSender with AnyWordSpecLike
  with Matchers with BeforeAndAfterAll with GivenWhenThen {

  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  private implicit val timeout: Timeout = Timeout(1000, TimeUnit.MILLISECONDS)
  private implicit val ec: ExecutionContext = ExecutionContext.global

  "Forestry Market" must {
    "Tree must be returned on request" in {
      Given("Prepared Forestry Market actor and request")
      val actor = system.actorOf(ForestryMarket.props, "test-forestry-market1")
      val request: ForestryMarket.TreeRequest = ForestryMarket.TreeRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Tree.getClass}")
      for {
        value <- response.mapTo[ForestryMarket.TreeResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Tree]
      }
    }

    "Wood must be returned on request" in {
      Given("Prepared Forestry Market actor and request")
      val actor = system.actorOf(ForestryMarket.props, "test-forestry-market2")
      val request: ForestryMarket.WoodRequest = ForestryMarket.WoodRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Wood.getClass}")
      for {
        value <- response.mapTo[ForestryMarket.WoodResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Wood]
      }
    }

    "Plank must be returned on request" in {
      Given("Prepared Forestry Market actor and request")
      val actor = system.actorOf(ForestryMarket.props, "test-forestry-market3")
      val request: ForestryMarket.PlankRequest = ForestryMarket.PlankRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Plank.getClass}")
      for {
        value <- response.mapTo[ForestryMarket.PlankResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Plank]
      }
    }
  }
}