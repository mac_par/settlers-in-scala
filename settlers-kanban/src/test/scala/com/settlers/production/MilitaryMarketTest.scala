package com.settlers.production

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import com.settlers.resources.military.{Shield, Soldier, Sword}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.{BeforeAndAfterAll, GivenWhenThen}

import scala.concurrent.ExecutionContext

class MilitaryMarketTest extends TestKit(ActorSystem("military-actor-test")) with ImplicitSender
  with AnyWordSpecLike with BeforeAndAfterAll with GivenWhenThen with Matchers {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  private implicit val timeout: Timeout = Timeout(1000, TimeUnit.MILLISECONDS)
  private implicit val ec: ExecutionContext = ExecutionContext.global

  "Military Market" must {
    "Sword must be returned on request" in {
      Given("Prepared Military Market actor and request")
      val actor = system.actorOf(MilitaryMarket.props, "test-military-market1")
      val request: MilitaryMarket.SwordRequest = MilitaryMarket.SwordRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Sword.getClass}")
      for {
        value <- response.mapTo[MilitaryMarket.SwordResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Sword]
      }
    }

    "Shield must be returned on request" in {
      Given("Prepared Military Market actor and request")
      val actor = system.actorOf(MilitaryMarket.props, "test-military-market2")
      val request: MilitaryMarket.ShieldRequest = MilitaryMarket.ShieldRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Shield.getClass}")
      for {
        value <- response.mapTo[MilitaryMarket.ShieldResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Shield]
      }
    }

    "Soldier must be returned on request" in {
      Given("Prepared Military Market actor and request")
      val actor = system.actorOf(MilitaryMarket.props, "test-Military-market3")
      val request: MilitaryMarket.SoldierRequest = MilitaryMarket.SoldierRequest()

      When("Request is performed")
      val response = actor ? request

      Then(s"Resource must be provided: ${Soldier.getClass}")
      for {
        value <- response.mapTo[MilitaryMarket.SoldierResponse]
      } {
        value mustNot be eq null
        value.resource mustNot be eq null
        value.resource mustBe a[Soldier]
      }
    }
  }
}
