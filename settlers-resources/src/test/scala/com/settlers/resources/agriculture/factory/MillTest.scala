package com.settlers.resources.agriculture.factory

import com.settlers.resources.agriculture.{Flour, Grain}
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class MillTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Mill creates a flour from a grain") {
    it("Flour creation test") {
      Given("Provided a mill and a grain instance")
      val mill = Mill()
      val grain = new Grain

      When("Mill receives a grain and produces flour")
      val result = mill.produce(grain)

      Then("Created instance is not null and of type Flour")
      result mustNot be eq (null)
      result mustBe a[Flour]
    }
  }

  //TODO: remove it or rework this test case - looked exception is not seen in the test
  /*import com.settlers.resources.MissingResourcesException
  describe("Mill throws MissingResourcesException on missing grain") {
    it("Flour creation test on missing resources") {
      Given("Provided a mill and a grain instance")
      val mill = Mill.apply()
      val grain = null

      When("Mill receives an empty grain")
      Then("Throws exception")
      val thrown = execute {mill.produce(grain)} must be isInstanceOf MissingResourcesException
      s"Required ${Grain.name} was not provided to ${Mill.name}}" mustBe eq(thrown.message)
    }
  }*/

  describe("Mill has a name") {
    it("Mill name test") {
      Given("Provided a mill expected name")
      val expectedResult = Mill.name
      val mill = Mill()

      When("Obtains name from toString method")
      val result = mill.toString
      Then("Names must be equal")
      result eq expectedResult
    }
  }
}
