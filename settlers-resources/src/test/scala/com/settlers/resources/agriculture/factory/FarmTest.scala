package com.settlers.resources.agriculture.factory

import com.settlers.resources.agriculture.Grain
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class FarmTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("A farm produces grain") {
    it("Farm should produce grain") {
      Given("A Farm instance")
      val farm: Farm = Farm()

      When("Farm produces its product")
      val result = farm.produce()

      Then("The result product is a grain instance")
      result mustNot equal(null)
      result mustBe a[Grain]
    }
  }

  describe("A Farm has its name") {
    it("Farm.name check") {
      Given("Expected name")
      val expectedName = Farm.name
      val farm = Farm()

      When("Invoking companion object method \"name\"")
      val result = farm.toString

      Then("Name should match")
      result eq expectedName
    }
  }
}
