package com.settlers.resources.agriculture.factory

import com.settlers.resources.agriculture.Water
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class WellTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("A well produces water") {
    it("Well should produce water") {
      Given("A Well instance")
      val well: Well = Well()
      val expectedResult = new Water

      When("Well produces its product")
      val result = well.produce()

      Then("The result product is a water instance")
      println(expectedResult)
      println(result)
      result mustNot equal(null)
      result mustBe a[Water]
    }
  }

  describe("A Well has its name") {
    it("Well.name check") {
      Given("Expected name")
      val expectedName = Well.name
      val well = Well()

      When("Retrieving name from toString method")
      val result = well.toString

      Then("Name should match")
      result eq expectedName
    }
  }
}
