package com.settlers.resources.agriculture

object AgricultureFactory {
  def grain: Grain = Grain()

  def water: Water = Water()

  def flour: Flour = Flour(grain)
}
