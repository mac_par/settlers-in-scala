package com.settlers.resources.military.factory

import com.settlers.resources.military.{MilitaryFactory, Soldier}
import com.settlers.resources.monetary.MonetaryFactory
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class BarracksTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Barracks tests") {
    it("Check on Barracks name") {
      Given("Initialized Barracks and expected result")
      val barracks = Barracks()
      val expectedResult = Barracks.name

      When("Obtain name from toString")
      val result = barracks.toString

      Then("Compare values")
      result mustNot be eq (null)
      result eq expectedResult
    }

    it("Barracks must produce coins from sword and coin") {
      Given("Initialized Barracks and expected result")
      val blackSmith = new Barracks
      val sword = MilitaryFactory.sword
      val coin = MonetaryFactory.coin
      val shield = MilitaryFactory.shield
      val expectedResult: Soldier = Soldier(sword, coin, shield)

      When("Produce Soldier")
      val result = blackSmith.produce((sword, coin, shield))

      Then("Compare products")
      result mustNot be eq (null)
      result mustBe a[Soldier]
      result eq expectedResult
    }

    /*
     import com.settlers.resources.MissingResourcesException
      it("Barracks on missing sword throws exception") {
      Given("Initialized Barracks, coin and uninitialized sword")
      val blackSmith = new Barracks
      val sword = null
      val coin = MonetaryFactory.coin

      When("Produce Soldier")
      Then("MissingResourcesException must be thrown on missing sword")
      val thrown = execute {blackSmith.produce((sword,coin)) } .mustBe a[MissingResourcesException]
    }

    it("Barracks on missing coin throws exception") {
      Given("Initialized Barracks, sword and uninitialized coin")
      val blackSmith = new Barracks
      val sword = MilitaryFactory.sword
      val coin = null

      When("Produce Soldier")
      Then("MissingResourcesException must be thrown on missing coin")
      val thrown = execute {blackSmith.produce((sword,coin)) }.mustBe a[MissingResourcesException]
    }*/
  }
}
