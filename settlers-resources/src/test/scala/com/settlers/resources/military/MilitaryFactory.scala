package com.settlers.resources.military

import com.settlers.resources.forestry.ForestryFactory
import com.settlers.resources.industrial.IndustryFactory.{coalOre, steel}
import com.settlers.resources.monetary.MonetaryFactory.coin

object MilitaryFactory {
  def sword: Sword = Sword(steel, coalOre)

  def shield: Shield = Shield(steel, coalOre, ForestryFactory.plank)

  def soldier: Soldier = Soldier(sword, coin, shield)
}
