package com.settlers.resources.military.factory

import com.settlers.resources.forestry.ForestryFactory
import com.settlers.resources.industrial.IndustryFactory
import com.settlers.resources.military.{Shield, Sword}
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class BlackSmithTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("BlackSmith tests") {
    it("Check on BlackSmith name") {
      Given("Initialized BlackSmith and expected result")
      val mint = BlackSmith()
      val expectedResult = BlackSmith.name

      When("Obtain name from toString")
      val result = mint.toString

      Then("Compare values")
      result mustNot be eq (null)
      result eq expectedResult
    }

    it("BlackSmith must produce coins from steel and coal ore") {
      Given("Initialized BlackSmith and expected result")
      val blackSmith = new BlackSmith
      val steel = IndustryFactory.steel
      val coal = IndustryFactory.coalOre
      val expectedResult: Sword = Sword(steel, coal)

      When("Produce Sword")
      val result = blackSmith.produce((steel, coal))

      Then("Compare products")
      result mustNot be eq (null)
      result mustBe a[Sword]
      result eq expectedResult
    }

    it("BlackSmith must produce a shield from steel, coal ore and plank") {
      Given("Initialized BlackSmith and expected result")
      val blackSmith = new BlackSmith
      val steel = IndustryFactory.steel
      val coal = IndustryFactory.coalOre
      val plank = ForestryFactory.plank
      val expectedResult: Shield = Shield(steel, coal, plank)

      When("Produce shield")
      val result = blackSmith.produce((steel, coal, plank))

      Then("Compare products")
      result mustNot be eq (null)
      result mustBe a[Shield]
      result eq expectedResult
    }

    /*
     import com.settlers.resources.MissingResourcesException
      it("Sword - BlackSmith on missing steel throws exception") {
      Given("Initialized BlackSmith, coal ore and uninitialized steel")
      val blackSmith = new BlackSmith
      val steel = null
      val coalOre = IndustrialFactory.coalOre

      When("Produce Sword")
      Then("MissingResourcesException must be thrown on missing steel")
      val thrown = execute {blackSmith.produce((steel,coalOre)) } .mustBe a[MissingResourcesException]
    }

    it("Sword - BlackSmith on missing coal ore throws exception") {
      Given("Initialized BlackSmith, steel and uninitialized coal ore")
      val blackSmith = new BlackSmith
      val steel = IndustrialFactory.steel
      val coalOre = null

      When("Produce Sword")
      Then("MissingResourcesException must be thrown on missing coalOre")
      val thrown = execute {blackSmith.produce((steel,coalOre)) }.mustBe a[MissingResourcesException]
    }

    it("Shield - BlackSmith on missing steel throws exception") {
      Given("Initialized BlackSmith, coal ore and uninitialized steel")
      val blackSmith = new BlackSmith
      val steel = null
      val coalOre = IndustrialFactory.coalOre
      val shield = null

      When("Produce Sword")
      Then("MissingResourcesException must be thrown on missing steel")
      val thrown = execute {blackSmith.produce((steel,coalOre, shield)) } .mustBe a[MissingResourcesException]
    }

    it("Shield - BlackSmith on missing coal ore throws exception") {
      Given("Initialized BlackSmith, steel and uninitialized coal ore")
      val blackSmith = new BlackSmith
      val steel = IndustrialFactory.steel
      val coalOre = null

      When("Produce Sword")
      Then("MissingResourcesException must be thrown on missing coalOre")
      val thrown = execute {blackSmith.produce((steel,coalOre, shield)) }.mustBe a[MissingResourcesException]
    }

    it("Shield - BlackSmith on missing shield throws exception") {
      Given("Initialized BlackSmith, steel,coal and uninitialized shield")
      val blackSmith = new BlackSmith
      val steel = IndustrialFactory.steel
      val coalOre = IndustryFactory.coalOre
      val shield = null

      When("Produce Sword")
      Then("MissingResourcesException must be thrown on missing coalOre")
      val thrown = execute {blackSmith.produce((steel,coalOre, shield)) }.mustBe a[MissingResourcesException]
    }
    */
  }
}
