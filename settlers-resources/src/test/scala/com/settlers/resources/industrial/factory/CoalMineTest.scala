package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.CoalOre
import com.settlers.resources.urban.UrbanFactory
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class CoalMineTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("CoalMine tests") {
    it("Check on CoalMine name") {
      Given("Initialized CoalMine and expected result")
      val coalMine = new CoalMine
      val expectedResult = CoalMine.name

      When("Obtain name from toString")
      val result = coalMine.toString

      Then("Compare values")
      result mustNot be eq (null)
      result eq expectedResult
    }

    it("Coal mine must produce Coal Ores from meat and beer") {
      Given("Initialized CoalMine and expected result")
      val coalMine = new CoalMine
      val meat = UrbanFactory.meat
      val beer = UrbanFactory.beer
      val expectedResult: CoalOre = CoalOre(meat, beer)

      When("Produce Coal Ore")
      val result = coalMine.produce((meat, beer))

      Then("Compare products")
      result mustNot be eq (null)
      result mustBe a[CoalOre]
      result eq expectedResult
    }

    /*
     import com.settlers.resources.MissingResourcesException
      it("Coal mine on missing meat throws exception") {
      Given("Initialized CoalMine, beer and uninitialized meat")
      val coalMine = new CoalMine
      val meat = null
      val beer = UrbanFactory.beer

      When("Produce Coal Ore")
      Then("MissingResourcesException must be thrown on missing meat")
      val thrown = execute {coalMine.produce((meat,beer)) } .mustBe a[MissingResourcesException]
    }

    it("Coal mine on missing beer throws exception") {
      Given("Initialized CoalMine, meat and uninitialized beer")
      val coalMine = new CoalMine
      val meat = UrbanFactory.meat
      val beer = null

      When("Produce Coal Ore")
      Then("MissingResourcesException must be thrown on missing beer")
      val thrown = execute {coalMine.produce((meat,beer)) }.mustBe a[MissingResourcesException]
    }*/
  }
}
