package com.settlers.resources.industrial

import com.settlers.resources.urban.UrbanFactory.{beer, bread, meat}

object IndustryFactory {
  def goldOre: GoldOre = GoldOre(bread, beer)

  def coalOre: CoalOre = CoalOre(meat, beer)

  def ironOre: IronOre = IronOre(meat, beer)

  def steel: Steel = Steel(coalOre, ironOre)
}
