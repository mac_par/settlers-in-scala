package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.IronOre
import com.settlers.resources.urban.UrbanFactory
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers


class IronMineTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("IronMine tests") {
    it("Check on IronMine name") {
      Given("Initialized IronMine and expected result")
      val ironMine = new IronMine
      val expectedResult = IronMine.name

      When("Obtain name from toString")
      val result = ironMine.toString

      Then("Compare values")
      result mustNot be eq (null)
      result eq expectedResult
    }

    it("Iron mine must produce Iron Ores from meat and beer") {
      Given("Initialized IronMine and expected result")
      val ironMine = new IronMine
      val meat = UrbanFactory.meat
      val beer = UrbanFactory.beer
      val expectedResult: IronOre = IronOre(meat, beer)

      When("Produce Iron Ore")
      val result = ironMine.produce((meat, beer))

      Then("Compare products")
      result mustNot be eq (null)
      result mustBe a[IronOre]
      result eq expectedResult
    }

    /*
     import com.settlers.resources.MissingResourcesException
      it("Iron mine on missing meat throws exception") {
      Given("Initialized IronMine, beer and uninitialized meat")
      val ironMine = new IronMine
      val meat = null
      val beer = UrbanFactory.beer

      When("Produce Iron Ore")
      Then("MissingResourcesException must be thrown on missing meat")
      val thrown = execute {ironMine.produce((meat,beer)) } .mustBe a[MissingResourcesException]
    }

    it("Iron mine on missing beer throws exception") {
      Given("Initialized IronMine, meat and uninitialized beer")
      val ironMine = new IronMine
      val meat = UrbanFactory.meat
      val beer = null

      When("Produce Iron Ore")
      Then("MissingResourcesException must be thrown on missing beer")
      val thrown = execute {ironMine.produce((meat,beer)) }.mustBe a[MissingResourcesException]
    }*/
  }
}
