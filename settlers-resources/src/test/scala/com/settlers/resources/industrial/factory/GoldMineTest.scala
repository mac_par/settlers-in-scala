package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.GoldOre
import com.settlers.resources.urban.UrbanFactory
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class GoldMineTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("GoldMine tests") {
    it("Check on GoldMine name") {
      Given("Initialized GoldMine and expected result")
      val goldMine = new GoldMine
      val expectedResult = GoldMine.name

      When("Obtain name from toString")
      val result = goldMine.toString

      Then("Compare values")
      result mustNot be eq(null)
      result eq expectedResult
    }

    it("Gold mine must produce Gold Ores from bread and beer") {
      Given("Initialized GoldMine and expected result")
      val goldMine = new GoldMine
      val bread = UrbanFactory.bread
      val beer = UrbanFactory.beer
      val expectedResult: GoldOre = GoldOre(bread, beer)

      When("Produce Gold Ore")
      val result = goldMine.produce((bread,beer))

      Then("Compare products")
      result mustNot be eq(null)
      result mustBe a[GoldOre]
      result eq expectedResult
    }

    /*
     import com.settlers.resources.MissingResourcesException
      it("Gold mine on missing bread throws exception") {
      Given("Initialized GoldMine, beer and uninitialized bread")
      val goldMine = new GoldMine
      val bread = null
      val beer = UrbanFactory.beer

      When("Produce Gold Ore")
      Then("MissingResourcesException must be thrown on missing bread")
      val thrown = execute {goldMine.produce((bread,beer)) } .mustBe a[MissingResourcesException]
    }

    it("Gold mine on missing beer throws exception") {
      Given("Initialized GoldMine, bread and uninitialized beer")
      val goldMine = new GoldMine
      val bread = UrbanFactory.bread
      val beer = null

      When("Produce Gold Ore")
      Then("MissingResourcesException must be thrown on missing beer")
      val thrown = execute {goldMine.produce((bread,beer)) }.mustBe a[MissingResourcesException]
    }*/
  }
}
