package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.{IndustryFactory, Steel}
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class SteelMillTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Steel Mill tests") {
    it("Check on Steel Mill name") {
      Given("Initialized Steel Mill and expected result")
      val steelMill = SteelMill
      val expectedResult = SteelMill.name

      When("Obtain name from toString")
      val result = steelMill.toString

      Then("Compare values")
      result mustNot be eq (null)
      result eq expectedResult
    }

    it("Steel Mill must produce steel from coal ores and iron ores") {
      Given("Initialized Steel Mill and expected result")
      val steelMill = new SteelMill
      val coalOre = IndustryFactory.coalOre
      val ironOre = IndustryFactory.ironOre
      val expectedResult: Steel = Steel(coalOre, ironOre)

      When("Produce steel")
      val result = steelMill.produce((coalOre, ironOre))

      Then("Compare products")
      result mustNot be eq (null)
      result mustBe a[Steel]
      result eq expectedResult
    }

    /*
     import com.settlers.resources.MissingResourcesException
      it("Steel Mill on missing coalOre throws exception") {
      Given("Initialized Steel Mill, ironOre and uninitialized coalOre")
      val steel Mill = new Steel Mill
      val coalOre = null
      val ironOre = IndustryFactory.ironOre

      When("Produce Coal Ore")
      Then("MissingResourcesException must be thrown on missing coalOre")
      val thrown = execute {steelMill.produce((coalOre,ironOre)) } .mustBe a[MissingResourcesException]
    }

    it("Steel Mill on missing ironOre throws exception") {
      Given("Initialized Steel Mill, coalOre and uninitialized ironOre")
      val steel Mill = new Steel Mill
      val coalOre = IndustryFactory.coalOre
      val ironOre = null

      When("Produce Coal Ore")
      Then("MissingResourcesException must be thrown on missing ironOre")
      val thrown = execute {steelMill.produce((coalOre,ironOre)) }.mustBe a[MissingResourcesException]
    }*/
  }
}
