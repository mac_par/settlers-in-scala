package com.settlers.resources.forestry

object ForestryFactory {
  def tree: Tree = Tree()
  def wood: Wood = Wood(tree)
  def plank: Plank = Plank(wood)
}
