package com.settlers.resources.forestry.factory

import com.settlers.resources.forestry.{Tree, Wood}
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class CutterTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Cutter creates wood from a tree") {
    it("Wood creation test") {
      Given("Provided a cutter and a tree instance")
      val cutter = Cutter()
      val tree = Tree()

      When("Cutter receives a tree and produces wood")
      val result = cutter.produce(tree)

      Then("Created instance of type Wood")
      result mustNot be eq (null)
      result mustBe a[Wood]
    }
  }

  //TODO: remove it or rework this test case - looked exception is not seen in the test
  /*import com.settlers.resources.MissingResourcesException
  describe("Cutter throws MissingResourcesException on missing tree") {
    it("Wood creation test on missing resources") {
      Given("Provided a cutter and a tree instance")
      val cutter = Cutter.apply()
      val tree = null

      When("Cutter receives an empty tree")
      Then("Throws exception")
      val thrown = execute {cutter.produce(tree)} must be isInstanceOf MissingResourcesException
      s"Required ${Tree.name} was not provided to ${Cutter.name}}" mustBe eq(thrown.message)
    }
  }*/

  describe("Cutter has a name") {
    it("Cutter name test") {
      Given("Provided a cutter expected name")
      val expectedResult = Cutter.name

      When("Cutter receives a tree and produces wood")
      val result = Cutter.name
      Then("Names must be equal")
      result eq expectedResult
    }
  }
}
