package com.settlers.resources.forestry.factory

import com.settlers.resources.forestry.Tree
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class ForestTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Forest produces tree") {
    it("Forest should produce tree") {
      Given("Forest instance")
      val farm: Forest = Forest()

      When("Forest produces its product")
      val result = farm.produce()

      Then("The result product is a tree instance")
      result mustNot equal(null)
      result mustBe a[Tree]
    }
  }

  describe("Forest has its name") {
    it("Forest.name check") {
      Given("Expected name")
      val expectedName = Forest.name

      When("Invoking companion object method \"name\"")
      val result = Forest.name

      Then("Name should match")
      result must equal(expectedName)
    }
  }
}
