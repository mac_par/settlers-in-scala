package com.settlers.resources.forestry.factory

import com.settlers.resources.forestry.{ForestryFactory, Plank}
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class SawMillTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("SawMill creates plank from a wood") {
    it("Wood creation test") {
      Given("Provided a sawMill and wood instance")
      val sawMill = SawMill()
      val wood = ForestryFactory.wood

      When("SawMill receives a wood and produces plank")
      val result = sawMill.produce(wood)

      Then("Created instance of type Plank")
      result mustNot be eq null
      result mustBe a[Plank]
    }
  }

  //TODO: remove it or rework this test case - looked exception is not seen in the test
  /*import com.settlers.resources.MissingResourcesException
  describe("SawMill throws MissingResourcesException on missing wood") {
    it("Plank creation test on missing resources") {
      Given("Provided a sawMill and empty resource")
      val sawMill = SawMill.apply()
      val wood = null

      When("SawMill receives an empty wood")
      Then("Throws exception")
      val thrown = execute {sawMill.produce(wood)} must be isInstanceOf MissingResourcesException
      s"Required ${Plank.name} was not provided to ${SawMill.name}}" mustBe eq(thrown.message)
    }
  }*/

  describe("SawMill has a name") {
    it("SawMill name test") {
      Given("Provided a sawMill expected name")
      val expectedResult = SawMill.name

      When("SawMill receives a wood and produces plank")
      val result = SawMill.name
      Then("Names must be equal")
      result eq expectedResult
    }
  }
}
