package com.settlers.resources.monetary.factory

import com.settlers.resources.industrial.IndustryFactory
import com.settlers.resources.monetary.Coin
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class MintTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Mint tests") {
    it("Check on Mint name") {
      Given("Initialized Mint and expected result")
      val mint = Mint()
      val expectedResult = Mint.name

      When("Obtain name from toString")
      val result = mint.toString

      Then("Compare values")
      result mustNot be eq (null)
      result eq expectedResult
    }

    it("Mint must produce coins from gold ore and coal ore") {
      Given("Initialized Mint and expected result")
      val mint = new Mint
      val gold = IndustryFactory.goldOre
      val coal = IndustryFactory.coalOre
      val expectedResult: Coin = Coin(gold, coal)

      When("Produce Coal Ore")
      val result = mint.produce((gold, coal))

      Then("Compare products")
      result mustNot be eq (null)
      result mustBe a[Coin]
      result eq expectedResult
    }

    /*
     import com.settlers.resources.MissingResourcesException
      it("Mint on missing gold ore throws exception") {
      Given("Initialized Mint, coal ore and uninitialized gold ore")
      val mint = new Mint
      val goldOre = null
      val coalOre = IndustrialFactory.coalOre

      When("Produce Coin")
      Then("MissingResourcesException must be thrown on missing goldOre")
      val thrown = execute {mint.produce((goldOre,coalOre)) } .mustBe a[MissingResourcesException]
    }

    it("Mint on missing coal ore throws exception") {
      Given("Initialized Mint, gold ore and uninitialized coal ore")
      val mint = new Mint
      val goldOre = IndustrialFactory.goldOre
      val coalOre = null

      When("Produce Coin")
      Then("MissingResourcesException must be thrown on missing coalOre")
      val thrown = execute {mint.produce((goldOre,coalOre)) }.mustBe a[MissingResourcesException]
    }*/
  }
}
