package com.settlers.resources.monetary

import com.settlers.resources.industrial.IndustryFactory.{coalOre, goldOre}

object MonetaryFactory {
  def coin: Coin = Coin(goldOre, coalOre)
}
