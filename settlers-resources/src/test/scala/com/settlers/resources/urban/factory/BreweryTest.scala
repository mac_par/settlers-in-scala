package com.settlers.resources.urban.factory

import com.settlers.resources.agriculture.{AgricultureFactory, Grain, Water}
import com.settlers.resources.urban.Beer
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class BreweryTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Brewery keeps its name") {
    it("Checking Brewery item name") {
      Given("Expected name")
      val expectedName = (new Brewery).toString

      When("Obtaining object's name")
      val result = Brewery.name
      Then("Names are equal")
      result mustNot be equals (null)
      result mustBe a[String]
      result eq expectedName
    }
  }

  describe("Brewery creates beer") {
    it("Checking whether Brewery creates beer") {
      Given("Creating Brewery, expected result and required resources")
      val brewery = new Brewery
      val grain: Grain = AgricultureFactory.grain
      val water: Water = AgricultureFactory.water
      val expectedResult: Beer = Beer(grain, water)

      When("Producing a meat")
      val result = brewery.produce(grain, water)
      Then("Result is not null, instance of Meat and equal to result")
      result mustNot be equals (null)
      result mustBe a[Beer]
      result eq expectedResult
    }
  }

  /*  import com.settlers.resources.MissingResourcesException
      describe("Brewery - exception case 1: Missing Grain") {
      it("On missing grain Brewery throws exception") {
        Given("Creating Brewery and empty grain")
        val brewery = new Brewery
        val grain = null

        When("An attempt to create beer")
        Then(s"Exception if thrown: ${MissingResourcesException.getClass}")
        val thrown = execute{brewery.produce(grain)}must be isInstanceOf MissingResourcesException
        s"Required ${Grain.name} was not provided to ${Brewery.name}}" mustBe eq(thrown.message)
      }
    }

       describe("Brewery - exception case 2: Missing Water") {
      it("On missing water Brewery throws exception") {
        Given("Creating Brewery and empty water")
        val brewery = new Brewery
        val water = null

        When("An attempt to create beer")
        Then(s"Exception if thrown: ${MissingResourcesException.getClass}")
        val thrown = execute{brewery.produce(water)}must be isInstanceOf MissingResourcesException
        s"Required ${Water.name} was not provided to ${Brewery.name}}" mustBe eq(thrown.message)
      }
    }*/
}
