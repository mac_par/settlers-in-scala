package com.settlers.resources.urban.factory


import com.settlers.resources.agriculture.{AgricultureFactory, Grain}
import com.settlers.resources.urban.Pig
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class PigstyTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Pigsty keeps its name") {
    it("Checking Pigstry item name") {
      Given("Expected name")
      val expectedName = Pigsty.name
      When("Obtaining object's name")
      val pigsty = (new Pigsty).toString
      Then("Names are equal")
      pigsty mustNot be equals (null)
      pigsty mustBe a[String]
      pigsty eq expectedName
    }
  }

  describe("Pigsty creates pig") {
    it("Checking whether Pigsty creates pig") {
      Given("Creating Pigsty, expected result and required resources")
      val pigsty = new Pigsty
      val grain: Grain = AgricultureFactory.grain
      val expectedResult = Pig(grain)

      When("Producing a pig")
      val result: Pig = pigsty.produce(grain)
      Then("Result is not null, instance of Pig and equal to result")
      result mustNot be equals (null)
      result mustBe a[Pig]
      result eq expectedResult
    }
  }

  /*  import com.settlers.resources.MissingResourcesException
      describe("Pigsty - exception case 1: Missing grain") {
      it("On missing grain Pigstry throws exception") {
        Given("Creating Pigsty and empty grain")
        val pigsty = new Pigsty
        val grain = null

        When("An attempt to create a pig")
        Then(s"Exception if thrown: ${MissingResourcesException.getClass}")
        val thrown = execute{pigsty.produce(grain)}must be isInstanceOf MissingResourcesException
        s"Required ${Grain.name} was not provided to ${Pigstry.name}}" mustBe eq(thrown.message)
      }
    }*/
}
