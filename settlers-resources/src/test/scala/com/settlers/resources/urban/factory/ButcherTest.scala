package com.settlers.resources.urban.factory

import com.settlers.resources.urban.{Meat, Pig, UrbanFactory}
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class ButcherTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Butcher keeps its name") {
    it("Checking Butcher item name") {
      Given("Expected name")
      val expectedName = Butcher.name
      When("Obtaining object's name")
      val butcher = (new Butcher).toString
      Then("Names are equal")
      butcher mustNot be equals (null)
      butcher mustBe a[String]
      butcher eq expectedName
    }
  }

  describe("Butcher creates meat") {
    it("Checking whether Butcher creates meat") {
      Given("Creating Butcher, expected result and required resources")
      val butcher = new Butcher
      val pig: Pig = UrbanFactory.pig
      val expectedResult = Meat(pig)

      When("Producing a meat")
      val result = butcher.produce(pig)
      Then("Result is not null, instance of Meat and equal to result")
      result mustNot be equals null
      result mustBe a[Meat]
      result eq expectedResult
    }
  }

  /*  import com.settlers.resources.MissingResourcesException
      describe("Butcher - exception case 1: Missing pig") {
      it("On missing pig Pigstry throws exception") {
        Given("Creating Butcher and empty pig")
        val butcher = new Butcher
        val pig = null

        When("An attempt to create a meat")
        Then(s"Exception if thrown: ${MissingResourcesException.getClass}")
        val thrown = execute{butcher.produce(pig)}must be isInstanceOf MissingResourcesException
        s"Required ${Pig.name} was not provided to ${Pigstry.name}}" mustBe eq(thrown.message)
      }
    }*/
}
