package com.settlers.resources.urban.factory

import com.settlers.resources.agriculture.{AgricultureFactory, Flour, Water}
import com.settlers.resources.urban.Bread
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class BakeryTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Bakery keeps its name") {
    it("Checking Bakery item name") {
      Given("Expected name")
      val expectedName = (new Bakery).toString

      When("Obtaining object's name")
      val result = Bakery.name
      Then("Names are equal")
      result mustNot be equals (null)
      result mustBe a[String]
      result eq expectedName
    }
  }

  describe("Bakery creates bread") {
    it("Checking whether Bakery creates bread") {
      Given("Creating Bakery, expected result and required resources")
      val bakery = new Bakery
      val flour: Flour = AgricultureFactory.flour
      val water: Water = AgricultureFactory.water
      val expectedResult = Bread(flour, water)

      When("Producing a bread")
      val result = bakery.produce(flour, water)
      Then("Result is not null, instance of Bread and equal to result")
      result mustNot be equals (null)
      result mustBe a[Bread]
      result eq expectedResult
    }
  }

  /*  import com.settlers.resources.MissingResourcesException
      describe("Bakery - exception case 1: Missing Flour") {
      it("On missing flour Bakery throws exception") {
        Given("Creating Bakery and empty flour")
        val Bakery = new Bakery
        val flour = null
        val water = null

        When("An attempt to create a bread")
        Then(s"Exception if thrown: ${MissingResourcesException.getClass}")
        val thrown = execute{bakery.produce(flour, water)}must be isInstanceOf MissingResourcesException
        s"Required ${Flour.name} was not provided to ${Bakery.name}}" mustBe eq(thrown.message)
      }
    }

       describe("Bakery - exception case 2: Missing Water") {
      it("On missing water Bakery throws exception") {
        Given("Creating Bakery and empty water")
        val Bakery = new Bakery
        val flour = AgricultureFactory.flour
        val water = null

        When("An attempt to create a bread")
        Then(s"Exception if thrown: ${MissingResourcesException.getClass}")
        val thrown = execute{bakery.produce(flour, water)}must be isInstanceOf MissingResourcesException
        s"Required ${Water.name} was not provided to ${Bakery.name}}" mustBe eq(thrown.message)
      }
    }*/
}
