package com.settlers.resources.urban

import com.settlers.resources.agriculture.AgricultureFactory

object UrbanFactory {
  def bread: Bread = new Bread(flour = AgricultureFactory.flour, water = AgricultureFactory.water)

  def beer: Beer = new Beer(AgricultureFactory.grain, AgricultureFactory.water)

  def pig: Pig = Pig(AgricultureFactory.grain)

  def meat: Meat = new Meat(pig)

}
