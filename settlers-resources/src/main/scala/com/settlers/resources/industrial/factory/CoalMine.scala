package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.CoalOre
import com.settlers.resources.urban.{Beer, Meat}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class CoalMine extends BiFactory[(Meat, Beer), CoalOre] with CheckableResource {
  override def produce(resource: (Meat, Beer)): CoalOre = {
    checkResource(resource._1, Meat, CoalOre)
    checkResource(resource._2, Meat, CoalOre)
    CoalOre(resource)
  }

  override def toString: String = CoalMine.name
}

object CoalMine extends Nameable {
  def apply(): CoalMine = new CoalMine()

  override def name: String = "Coal Mine"
}
