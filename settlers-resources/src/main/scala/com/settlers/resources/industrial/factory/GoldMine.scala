package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.GoldOre
import com.settlers.resources.urban.{Beer, Bread}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class GoldMine extends BiFactory[(Bread, Beer), GoldOre] with CheckableResource {
  override def produce(resource: (Bread, Beer)): GoldOre = {
    checkResource(resource._1, Bread, GoldMine)
    checkResource(resource._2, Beer, GoldMine)
    GoldOre(resource)
  }

  override def toString: String = GoldMine.name
}

object GoldMine extends Nameable {
  def apply(): GoldMine = new GoldMine()

  override def name: String = "Gold Mine"
}
