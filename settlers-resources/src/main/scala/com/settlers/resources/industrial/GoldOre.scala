package com.settlers.resources.industrial

import com.settlers.resources.urban.{Beer, Bread}
import com.settlers.resources.{Nameable, Resource}

final class GoldOre private[industrial](private val bread: Bread, private val beer: Beer) extends Resource {
  override def toString: String = GoldOre.name
}

object GoldOre extends Nameable {
  private[industrial] def apply(bread: Bread, beer: Beer): GoldOre = new GoldOre(bread, beer)

  private[industrial] def apply(resource: (Bread, Beer)): GoldOre = new GoldOre(resource._1, resource._2)

  override def name: String = "Gold Ore"
}
