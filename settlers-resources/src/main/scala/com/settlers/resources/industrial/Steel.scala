package com.settlers.resources.industrial

import com.settlers.resources.{Nameable, Resource}

final class Steel private[industrial](private val coalOre: CoalOre, private val ironOre: IronOre) extends Resource {

  override def toString: String = Steel.name
}

object Steel extends Nameable {
  private[industrial] def apply(coalOre: CoalOre, ironOre: IronOre): Steel = new Steel(coalOre, ironOre)

  private[industrial] def apply(resource: (CoalOre, IronOre)): Steel = new Steel(resource._1, resource._2)

  override def name: String = "Steel"
}
