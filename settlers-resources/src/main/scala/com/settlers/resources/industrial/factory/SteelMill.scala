package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.{CoalOre, IronOre, Steel}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class SteelMill extends BiFactory[(CoalOre, IronOre), Steel] with CheckableResource {
  override def produce(resource: (CoalOre, IronOre)): Steel = {
    checkResource(resource._1, CoalOre, SteelMill)
    checkResource(resource._2, IronOre, SteelMill)
    Steel(resource)
  }

  override def toString: String = SteelMill.name
}

object SteelMill extends Nameable {
  def apply(): SteelMill = new SteelMill()

  override def name: String = "Steel Mill"
}
