package com.settlers.resources.industrial

import com.settlers.resources.urban.{Beer, Meat}
import com.settlers.resources.{Nameable, Resource}

final class IronOre private[industrial](private val meat: Meat, private val beer: Beer) extends Resource {
  override def toString: String = IronOre.name
}

object IronOre extends Nameable {
  private[industrial] def apply(meat: Meat, beer: Beer): IronOre = new IronOre(meat, beer)

  private[industrial] def apply(resource: (Meat, Beer)): IronOre = new IronOre(resource._1, resource._2)

  override def name: String = "Iron Ore"
}
