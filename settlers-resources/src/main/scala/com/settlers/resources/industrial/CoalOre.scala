package com.settlers.resources.industrial

import com.settlers.resources.urban.{Beer, Meat}
import com.settlers.resources.{Nameable, Resource}

final class CoalOre private[industrial](private val meat: Meat, private val beer: Beer) extends Resource {
  override def toString: String = CoalOre.name
}

object CoalOre extends Nameable {
  override def name: String = "Coal Ore"

  private[industrial] def apply(meat: Meat, beer: Beer): CoalOre = new CoalOre(meat, beer)

  private[industrial] def apply(resource: (Meat, Beer)): CoalOre = new CoalOre(resource._1, resource._2)
}
