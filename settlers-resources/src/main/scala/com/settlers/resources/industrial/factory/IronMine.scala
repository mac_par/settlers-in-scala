package com.settlers.resources.industrial.factory

import com.settlers.resources.industrial.IronOre
import com.settlers.resources.urban.{Beer, Meat}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class IronMine extends BiFactory[(Meat, Beer), IronOre] with CheckableResource {
  override def produce(resource: (Meat, Beer)): IronOre = {
    checkResource(resource._1, Meat, IronMine)
    checkResource(resource._2, Beer, IronMine)
    IronOre(resource)
  }

  override def toString: String = IronMine.name
}

object IronMine extends Nameable {
  def apply(): IronMine = new IronMine()

  override def name: String = "Iron Mine"
}
