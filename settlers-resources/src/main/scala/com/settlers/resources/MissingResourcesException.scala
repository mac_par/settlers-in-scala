package com.settlers.resources

final class MissingResourcesException private[resources](msg: String, cause: Throwable)
  extends Exception(msg, cause) {

  def this(resource: String, factory: String, cause: Throwable = null) = this(s"Required $resource was not provided to $factory", cause)

  def this(msg: () => String, cause: Throwable) = this(msg(), cause)
}
