package com.settlers.resources.military.factory

import com.settlers.resources.forestry.Plank
import com.settlers.resources.industrial.{CoalOre, Steel}
import com.settlers.resources.military.{Shield, Sword}
import com.settlers.resources.{CheckableResource, Nameable}

final class BlackSmith extends CheckableResource {
  def produce(resource: (Steel, CoalOre)): Sword = {
    checkResource(resource._1, Steel, BlackSmith)
    checkResource(resource._2, CoalOre, BlackSmith)
    Sword(resource)
  }

  def produce(resource: (Steel, CoalOre, Plank)): Shield = {
    checkResource(resource._1, Steel, BlackSmith)
    checkResource(resource._2, CoalOre, BlackSmith)
    checkResource(resource._3, Plank, BlackSmith)
    Shield(resource)
  }

  override def toString: String = BlackSmith.name
}

object BlackSmith extends Nameable {
  def apply(): BlackSmith = new BlackSmith()

  override def name: String = "BlackSmith"
}
