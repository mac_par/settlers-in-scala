package com.settlers.resources.military

import com.settlers.resources.forestry.Plank
import com.settlers.resources.industrial.{CoalOre, Steel}
import com.settlers.resources.{Nameable, Resource}

final class Shield private[military](private val steel: Steel, private val coalOre: CoalOre, private val plank: Plank)
  extends Resource {
  override def toString: String = Shield.name
}

object Shield extends Nameable {
  private[military] def apply(steel: Steel, coalOre: CoalOre, plank: Plank) = new Shield(steel, coalOre, plank)

  private[military] def apply(resource: (Steel, CoalOre, Plank)) = new Shield(resource._1, resource._2, resource._3)

  override def name: String = "Shield"
}
