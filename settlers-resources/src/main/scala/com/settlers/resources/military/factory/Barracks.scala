package com.settlers.resources.military.factory

import com.settlers.resources.military.{Shield, Soldier, Sword}
import com.settlers.resources.monetary.Coin
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Barracks extends BiFactory[(Sword, Coin, Shield), Soldier] with CheckableResource {
  override def produce(resource: (Sword, Coin, Shield)): Soldier = {
    checkResource(resource._1, Sword, Barracks)
    checkResource(resource._2, Coin, Barracks)
    checkResource(resource._3, Shield, Barracks)
    Soldier(resource)
  }

  override def toString: String = Barracks.name
}

object Barracks extends Nameable {
  def apply(): Barracks = new Barracks()

  override def name: String = "Barracks"
}



