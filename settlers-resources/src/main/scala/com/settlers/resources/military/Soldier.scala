package com.settlers.resources.military

import com.settlers.resources.monetary.Coin
import com.settlers.resources.{Nameable, Resource}

final class Soldier private[military](private val sword: Sword, private val coin: Coin, private val shield: Shield) extends Resource {
  override def toString: String = Soldier.name
}

object Soldier extends Nameable {
  private[military] def apply(sword: Sword, coin: Coin, shield: Shield): Soldier = new Soldier(sword, coin, shield)

  private[military] def apply(resource: (Sword, Coin, Shield)): Soldier = new Soldier(resource._1, resource._2, resource._3)

  override def name: String = "Soldier"
}

