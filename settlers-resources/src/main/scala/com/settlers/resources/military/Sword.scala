package com.settlers.resources.military

import com.settlers.resources.industrial.{CoalOre, Steel}
import com.settlers.resources.{Nameable, Resource}

final class Sword private[military](private val steel: Steel, private val coal: CoalOre) extends Resource {
  override def toString: String = Sword.name
}

object Sword extends Nameable {
  private[military] def apply(steel: Steel, coal: CoalOre): Sword = new Sword(steel, coal)

  private[military] def apply(resource: (Steel, CoalOre)): Sword = new Sword(resource._1, resource._2)

  override def name: String = "Sword"
}
