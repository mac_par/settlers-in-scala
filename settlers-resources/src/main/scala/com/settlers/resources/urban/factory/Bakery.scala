package com.settlers.resources.urban.factory

import com.settlers.resources.agriculture.{Flour, Water}
import com.settlers.resources.urban.Bread
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Bakery extends BiFactory[(Flour, Water), Bread] with CheckableResource {
  override def produce(resource: (Flour, Water)): Bread = {
    checkResource(resource._1, Flour, Bakery)
    checkResource(resource._2, Water, Bakery)
    Bread(resource)
  }

  override def toString: String = Bakery.name
}

object Bakery extends Nameable {
  def apply(): Bakery = new Bakery()

  override def name: String = "Brewery"
}
