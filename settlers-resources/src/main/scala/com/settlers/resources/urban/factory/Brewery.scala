package com.settlers.resources.urban.factory

import com.settlers.resources.agriculture.{Grain, Water}
import com.settlers.resources.urban.Beer
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Brewery extends BiFactory[(Grain, Water), Beer] with CheckableResource {
  override def produce(resource: (Grain, Water)): Beer = {
    checkResource(resource._1, Grain, Brewery)
    checkResource(resource._2, Water, Brewery)
    Beer(resource)
  }

  override def toString: String = Brewery.name
}

object Brewery extends Nameable {
  def apply(): Brewery = new Brewery()

  override def name: String = "Brewerey"
}
