package com.settlers.resources.urban.factory

import com.settlers.resources.agriculture.Grain
import com.settlers.resources.urban.Pig
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Pigsty extends BiFactory[Grain, Pig] with CheckableResource {
  override def produce(resource: Grain): Pig = {
    checkResource(resource, Grain, Pigsty)
    Pig(resource)
  }

  override def toString: String = Pigsty.name
}

object Pigsty extends Nameable {
  def apply(): Pigsty = new Pigsty()

  override def name: String = "Pigsty"
}