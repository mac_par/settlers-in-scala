package com.settlers.resources.urban

import com.settlers.resources.agriculture.Grain
import com.settlers.resources.{Nameable, Resource}

final class Pig private[urban](private val grain: Grain) extends Resource {
  override def toString: String = Pig.name
}

object Pig extends Nameable {
  private[urban] def apply(grain: Grain): Pig = new Pig(grain)

  override def name: String = "Pig"
}