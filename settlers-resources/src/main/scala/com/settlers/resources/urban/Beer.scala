package com.settlers.resources.urban

import com.settlers.resources.agriculture.{Grain, Water}
import com.settlers.resources.{Nameable, Resource}

final class Beer private[urban](private val grain: Grain, private val water: Water) extends Resource {
  override def toString: String = Beer.name
}

object Beer extends Nameable {
  private[urban] def apply(grain: Grain, water: Water): Beer = new Beer(grain, water)

  private[urban] def apply(resources: (Grain, Water)): Beer = new Beer(resources._1, resources._2)

  override def name: String = "Beer"
}
