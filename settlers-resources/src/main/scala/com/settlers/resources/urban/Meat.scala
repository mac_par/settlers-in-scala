package com.settlers.resources.urban

import com.settlers.resources.{Nameable, Resource}

final class Meat private[urban](private val pig: Pig) extends Resource {
  override def toString: String = Meat.name
}

object Meat extends Nameable {
  private[urban] def apply(pig: Pig): Meat = new Meat(pig)

  override def name: String = "Meat"
}