package com.settlers.resources.urban.factory

import com.settlers.resources.urban.{Meat, Pig}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Butcher extends BiFactory[Pig, Meat] with CheckableResource {
  override def produce(resource: Pig): Meat = {
    checkResource(resource, Pig, Butcher)
    Meat(resource)
  }

  override def toString: String = Butcher.name
}

object Butcher extends Nameable {
  def apply(): Butcher = new Butcher()

  override def name: String = "Butcher"
}