package com.settlers.resources.urban

import com.settlers.resources.agriculture.{Flour, Water}
import com.settlers.resources.{Nameable, Resource}

final class Bread private[urban](private val flour: Flour, private val water: Water) extends Resource {
  override def toString: String = Bread.name
}

object Bread extends Nameable {
  private[urban] def apply(flour: Flour, water: Water): Bread = new Bread(flour, water)

  private[urban] def apply(resource: (Flour, Water)): Bread = new Bread(resource._1, resource._2)

  override def name: String = "Bread"
}
