package com.settlers.resources.forestry.factory

import com.settlers.resources.forestry.{Plank, Wood}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class SawMill extends BiFactory[Wood, Plank] with CheckableResource {
  override def produce(resource: Wood): Plank = {
    checkResource(resource, Wood, SawMill)
    Plank(resource)
  }

  override def toString: String = SawMill.name
}

object SawMill extends Nameable {
  def apply(): SawMill = new SawMill()

  override def name: String = "SawMill"
}
