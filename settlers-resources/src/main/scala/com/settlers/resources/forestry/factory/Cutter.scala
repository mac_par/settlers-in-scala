package com.settlers.resources.forestry.factory

import com.settlers.resources.forestry.{Tree, Wood}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Cutter extends BiFactory[Tree, Wood] with CheckableResource {
  override def produce(resource: Tree): Wood = {
    checkResource(resource, Tree, Cutter)
    Wood(resource)
  }

  override def toString: String = Cutter.name
}

object Cutter extends Nameable {
  def apply(): Cutter = new Cutter()

  override def name: String = "Cutter"
}
