package com.settlers.resources.forestry.factory

import com.settlers.resources.forestry.Tree
import com.settlers.resources.{Factory, Nameable}

final class Forest extends Factory[Tree] {
  override def produce(): Tree = Tree()

  override def toString: String = Forest.name
}

object Forest extends Nameable {
  def apply(): Forest = new Forest()

  override def name: String = "Pine Forest"
}
