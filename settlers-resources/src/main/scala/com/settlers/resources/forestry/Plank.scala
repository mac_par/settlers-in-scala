package com.settlers.resources.forestry

import com.settlers.resources.{Nameable, Resource}

final class Plank private[forestry](private val wood: Wood) extends Resource {
  override def toString: String = Plank.name
}

object Plank extends Nameable {
  private[forestry] def apply(wood: Wood): Plank = new Plank(wood)

  override def name: String = "Plank"
}
