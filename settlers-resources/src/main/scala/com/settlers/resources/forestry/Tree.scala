package com.settlers.resources.forestry

import com.settlers.resources.{Nameable, Resource}

final class Tree private[forestry] extends Resource {
  override def toString: String = Tree.name
}

object Tree extends Nameable {
  private[forestry] def apply(): Tree = new Tree()

  override def name: String = "Tree"
}
