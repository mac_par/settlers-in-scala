package com.settlers.resources.forestry

import com.settlers.resources.{Nameable, Resource}

final class Wood private[forestry](private val tree: Tree) extends Resource {
  override def toString: String = Wood.name
}

object Wood extends Nameable {
  private[forestry] def apply(tree: Tree): Wood = new Wood(tree)

  override def name: String = "Wood"
}
