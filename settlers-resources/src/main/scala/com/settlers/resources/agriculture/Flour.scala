package com.settlers.resources.agriculture

import com.settlers.resources.{Nameable, Resource}

final class Flour private[agriculture](private val grain: Grain) extends Resource {
  override def toString: String = Flour.name
}

object Flour extends Nameable {
  private[agriculture] def apply(grain: Grain): Flour = new Flour(grain)

  override def name: String = "Flour"
}

