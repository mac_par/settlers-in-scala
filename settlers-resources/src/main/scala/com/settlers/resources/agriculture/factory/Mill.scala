package com.settlers.resources.agriculture.factory

import com.settlers.resources.agriculture.{Flour, Grain}
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Mill extends BiFactory[Grain, Flour] with CheckableResource {
  override def produce(grain: Grain): Flour = {
    checkResource(grain, Grain, Mill)
    new Flour(grain)
  }

  override def toString: String = Mill.name
}

object Mill extends Nameable {
  def apply() = new Mill()

  override def name: String = "Mill"
}


