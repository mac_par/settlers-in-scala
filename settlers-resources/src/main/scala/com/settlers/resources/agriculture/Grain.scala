package com.settlers.resources.agriculture

import com.settlers.resources.{Nameable, Resource}

final class Grain private[agriculture] extends Resource {
  override def toString: String = Grain.name
}

object Grain extends Nameable {
  private[agriculture] def apply(): Grain = new Grain()

  override def name: String = "Grain"
}
