package com.settlers.resources.agriculture.factory

import com.settlers.resources.agriculture.Water
import com.settlers.resources.{Factory, Nameable}

final class Well extends Factory[Water] {
  override def produce(): Water = new Water

  override def toString: String = Well.name
}

object Well extends Nameable {
  def apply(): Well = new Well()

  override def name: String = "Well"
}
