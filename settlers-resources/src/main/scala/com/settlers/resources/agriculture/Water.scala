package com.settlers.resources.agriculture

import com.settlers.resources.{Nameable, Resource}

final class Water private[agriculture] extends Resource {
  override def toString: String = Water.name
}

object Water extends Nameable {
  private[agriculture] def apply(): Water = new Water()

  override def name: String = "Water"
}