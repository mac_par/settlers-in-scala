package com.settlers.resources.agriculture.factory

import com.settlers.resources.agriculture.Grain
import com.settlers.resources.{Factory, Nameable}

final class Farm extends Factory[Grain] {
  override def produce(): Grain = new Grain

  override def toString: String = Farm.name
}


object Farm extends Nameable {
  def apply(): Farm = new Farm()

  override def name: String = "Farm"
}
