package com.settlers.resources.monetary.factory

import com.settlers.resources.industrial.{CoalOre, GoldOre}
import com.settlers.resources.monetary.Coin
import com.settlers.resources.{BiFactory, CheckableResource, Nameable}

final class Mint extends BiFactory[(GoldOre, CoalOre), Coin] with CheckableResource {
  override def produce(resource: (GoldOre, CoalOre)): Coin = {
    checkResource(resource._1, GoldOre, Mint)
    checkResource(resource._2, CoalOre, Mint)
    Coin(resource)
  }

  override def toString: String = Mint.name
}

object Mint extends Nameable {
  def apply(): Mint = new Mint()

  override def name: String = "Mint"
}
