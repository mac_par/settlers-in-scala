package com.settlers.resources.monetary

import com.settlers.resources.industrial.{CoalOre, GoldOre}
import com.settlers.resources.{Nameable, Resource}

final class Coin private[monetary](private val goldOre: GoldOre, private val coalOre: CoalOre) extends Resource {
  override def toString: String = Coin.name
}

object Coin extends Nameable {
  private[monetary] def apply(goldOre: GoldOre, coalOre: CoalOre): Coin = new Coin(goldOre, coalOre)

  private[monetary] def apply(resource: (GoldOre, CoalOre)): Coin = new Coin(resource._1, resource._2)

  override def name: String = "Coin"
}
