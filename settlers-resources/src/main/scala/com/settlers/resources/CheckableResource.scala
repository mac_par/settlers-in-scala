package com.settlers.resources

import java.util.Objects

trait CheckableResource {
  protected def checkResource(resource: Resource, resourceName: Nameable, factory: Nameable): Unit = {
    if (Objects.isNull(resource)) {
      throw new MissingResourcesException(resourceName.name, factory.name)
    }
  }
}
