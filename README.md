**War Lord App**  
_War Lord App_ provide functionality to concurrently produce Soldiers,  
in order to send them to the battlefield.  

In order to obtain a single soldier, the following steps are requested:
1. run `mvn install` from command line in order to download all required
libraries and install **War Lord App** into local respository
2. Import the library, by adding below entry into `dependencies` tag  
```xml
<dependency>
  <groupId>com.settlers</groupId>  
  <artifactId>settlers-kanban</artifactId>  
  <version>required verion</version>  
 </dependency>
```

3. Import WorldApp instance and use it as follows:  
`
val warLord = new WarLord
val promisedSoldier = warLord.promiseMeASoldat
`

Afterwards received promise should be unpacked and used.

4. When you are done using current war lord instance, please terminate it using `close` method.
In case of need for another `Soldier` instances please follow the well-known rule: _When current war lord was terminated, an another one will take his place_
  
  *****
  Currently provided version is based on Kanban. However, there are plans to optimize it and add additional queue-based counterpart, إن شاء الله
    
_Test cases_  
Provided test cases are organised in ScalaTest and Akka TestKit. However, current configuration does not provide full integration with Maven, so in result during test stage all tests are skipped.
Otherwise, all test cases would be run, but without any result.